@extends('admin.master')

@section('title')
    Mediusware | View
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Emp. Note View</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('emp-note.index') !!}">Emp. Note</a></li>
                        <li class="breadcrumb-item active" aria-current="page">View</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

        @include('admin.include.alert')

        <div class="row">
            <!-- Column -->
            <div class="col-lg-4 col-xlg-3 col-md-5">
                <div class="card">
                    <div class="card-body">
                        <center class="mt-4">
                            @if(!empty($data->user))
                                @if($data->user->image)
                                    <img src="{!! asset('media/user/'. $data->user->image) !!}" class="rounded-circle" width="150">
                                @else
                                    <img src="{!! asset('avatar.png') !!}" class="rounded-circle" width="150">
                                @endif
                            @endif


                            <h4 class="card-title mt-2">{!! $data->user->name !!}</h4>
                            <h6 class="card-subtitle">
                                @if($data->user->role == 'employee')
                                    Employee
                                @endif
                            </h6>

                        </center>
                    </div>
                    <div>
                        <hr> </div>
                    <div class="card-body"> <small class="text-muted">Email address </small>
                        <h6>{!! $data->user->email !!}</h6> <small class="text-muted pt-4 db">Phone</small>
                        <h6>{!! $data->user->phone !!}</h6> <small class="text-muted pt-4 db">About</small>
                        <h6 class="text-justify">{!! $data->user->about !!}</h6>
                    </div>
                </div>
            </div>
            <!-- Column -->
            <!-- Column -->
            <div class="col-lg-8 col-xlg-9 col-md-7">
                <div class="card">
                    <div class="card-body">
                        <h3 class="text-info">Date:</h3>
                        <p>{{ date('d M Y', strtotime($data->date)) }}</p>


                        <h3 class="text-info">Comment:</h3>
                        <p>{!! $data->comment !!}</p>
                    </div>
                </div>
            </div>
            <!-- Column -->
        </div>

        <!-- First Cards Row  -->
    </div>


@endsection

@section('page_js')
@endsection
