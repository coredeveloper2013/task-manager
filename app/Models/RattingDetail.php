<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class RattingDetail extends Model
{
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function rattingMaster()
    {
        return $this->belongsTo(Ratting::class, 'ratting_id', 'id');
    }
}
