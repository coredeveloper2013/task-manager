@extends('admin.master')

@section('title')
    Mediusware | Rattings
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Rattings</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Rattings</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

    @include('admin.include.alert')
    <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">Rattings Report</h4>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>
                        <form action="{!! url('taskman/ratting-report') !!}" method="get">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>Start Date</h5>
                                        <div class="input-group">
                                            <input type="text"
                                                   class="form-control{{ $errors->has('start_date') ? ' is-invalid' : '' }} datepicker-autoclose"
                                                   value="{!! !empty($data) ? $data['start_date']:"" !!}"
                                                   name="start_date" placeholder="Start Date">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>End Date</h5>
                                        <div class="input-group">
                                            <input type="text"
                                                   class="form-control{{ $errors->has('end_date') ? ' is-invalid' : '' }} datepicker-autoclose"
                                                   value="{!! !empty($data) ? $data['end_date']:"" !!}"
                                                   name="end_date" placeholder="End Date">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>Select Employee</h5>
                                        <div class="controls">
                                            <select class="form-control" name="user_id">
                                                <option value="">All Employees</option>
                                                @foreach($employees as $user)
                                                    <option
                                                        value="{!! $user->id !!}" {!! !empty($data) && $data['user_id'] == $user->id ? 'selected':''  !!}>{!! $user->name !!}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>Select Manager</h5>
                                        <div class="controls">
                                            <select class="form-control" name="manager_id">
                                                <option value="">All Employees</option>
                                                @foreach($managers as $manager)
                                                    <option
                                                        value="{!! $manager->id !!}" {!! !empty($data) && $data['manager_id'] == $manager->id ? 'selected':''  !!}>{!! $manager->name !!}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>Select Ratting</h5>
                                        <div class="controls">
                                            <select
                                                class="form-control{{ $errors->has('ratting') ? ' is-invalid' : '' }}"
                                                name="ratting">
                                                <option value=""> Select Ratting</option>
                                                @for ($star = 1; $star <= 10; $star++)
                                                    <option
                                                        {!! !empty($data) && $data['ratting'] == $star ? 'selected':''  !!} value="{{ $star }}"> {{ $star }}
                                                        *
                                                    </option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <br>
                                        <div class="controls">
                                            <button type="submit" class="btn btn-info">Go</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table id="row_create_call" class="table table-striped table-hover table-bordered display"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>Manager Name</th>
                                    <th>Date</th>
                                    <th>Employee</th>
                                    <th>Ratting</th>
                                    <th>Note</th>
                                </tr>
                                </thead>
                                <tbody id="tablecontents">
                                @php
                                    $totalRatting = 0;
                                @endphp
                                @foreach($rattings as $key => $ratting)
                                    <tr class="row1" data-id="{{ $ratting->id }}">
                                        <td>{!! ++$key !!}</td>
                                        <td>{!! $ratting->rattingMaster->user->name !!}</td>
                                        <td>{!! date('F d, Y', strtotime($ratting->rattingMaster->date)) !!}</td>
                                        <td>{!! $ratting->user->name !!}</td>
                                        <td> {{ $ratting->ratting }}</td>
                                        <td> {!! $ratting->comment !!} </td>
                                        @php
                                            $totalRatting += $ratting->ratting;
                                        @endphp
                                    </tr>
                                @endforeach
                                </tbody>
                                @if(count($rattings)>0)
                                    <tfooter>
                                        <tr>
                                            <th colspan="4" class="text-center">Ratting %</th>
                                            <th> {{ number_format(($totalRatting*10) / (count($rattings)), 2) }} %</th>
                                            <th></th>
                                        </tr>
                                    </tfooter>
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('page_js')
    <script type="text/javascript">
    </script>
@endsection
