<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class BackupMysql extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:mysql';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Mysql Backup';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //Call Controller function...
        return app('App\Http\Controllers\BackupMySqlController')->sendCorn();
        $this->info('The backup has been proceed successfully.');
    }
}
