@extends('layouts.mail')

@section('mail-content')
    <div style="padding: 40px; background: #fff;">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tbody>
            <tr>
                <td>
                    <b>Hello {!! $employee_name !!}</b>
                    <p>Date: {!! $start_date !!} - {!! $date_to !!} ({!! $numberDays !!} Day)</p>
                    <span>Subject: {!! $subject !!}</span><br>
                    <span>{!! $body !!}</span><br>
                    <span><b>Leave Type: </b>{!! $leave_type == 'casual_leave' ? 'Casual Leave':'Sick Leave' !!}</span><br>

                    <span style="background: #F1F1F1">
                        <b>Note: </b>
                        @if($author_role == 'management')
                            <i>Management {!! $status !!}</i>
                        @else
                            <i>Admin {!! $status !!}</i>
                        @endif
                    </span>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection
