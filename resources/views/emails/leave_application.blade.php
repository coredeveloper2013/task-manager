@extends('layouts.mail')

@section('mail-content')
    <div style="padding: 40px; background: #fff;">
        <table border="0" cellpadding="0" cellspacing="0" style="width: 100%;">
            <tbody>
            <tr>
                <td><b>Hello Admin or Management</b>
                    <p>Date: {!! $start_date !!} - {!! $date_to !!} ({!! $numberDays !!} Day)</p>
                    <span>Subject: {!! $subject !!}</span><br>
                    <span>{!! $body !!}</span><br>
                    <span><b>Leave Type: </b>{!! $leave_type !!}</span><br>
                    <span>Yours Sincerely,</span><br>
                    <span><b>{!! $employee_name !!}</b></span>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
@endsection
