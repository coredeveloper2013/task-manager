@extends('admin.master')

@section('title')
    Mediusware | Interview Exams
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">{{ $exam->title }} Exam</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{ $exam->title }} Exam</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->

    <!-- Container fluid  -->
    <div class="page-content container-fluid" id="question_vue">

    @include('admin.include.alert')
    <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">Exams Questions</h4>
                            </div>
                            <div class="col-md-2 text-right">
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="list-group" v-for="(question, index) in questions">
                                    <div class="list-group-item">
                                        <div class="row">
                                            <div class="col-md-11">
                                                <b>@{{ index+1 }}. @{{ question.question }}</b>
                                                <div class="float-right" v-if="question.image">
                                                    <span v-html="makeImageUrl(question.image)"></span>
                                                    <button type="button" class="btn btn-circle btn-danger btn-sm"
                                                            @click="deleteImage(question.id)">
                                                        <i class="fas fa-trash-alt"></i>
                                                    </button>
                                                </div>
                                                <p v-html="question.description"></p>
                                            </div>
                                            <div class="col-md-1">
                                                <div class="float-right">
                                                    <button @click="editQuestion(question.id)"
                                                            type="button"
                                                            class="btn btn-info btn-circle update-button"
                                                            data-toggle="modal"
                                                            data-target="#update-question-modal">
                                                        <i class="fa fa-edit"></i>
                                                    </button>
                                                    <button @click="deleteQuestionModal(question.id)"
                                                            type="button"
                                                            class="btn btn-circle btn-danger"><i
                                                            class="fas fa-trash-alt"></i></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-3">
                            <div class="col-12">
                                <form id="create-question-form" @submit.prevent="createQuestions">
                                    <input type="hidden" name="exam_id" value="{{ $exam->id }}">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="form-group">
                                                <input class="form-control" type="text" name="question"
                                                       v-model="question.question"
                                                       placeholder="Question Heading">
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group">
                                                <input class="form-control" type="file" name="image">
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <textarea class="form-control summernote" name="description"
                                                          placeholder="Question Description"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <button type="submit" class="btn btn-success">
                                                <i class="fa fa-plus-circle"></i> Add New
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Update Question modal content -->
        <div id="update-question-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="vcenter">Update Question</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <form id="update-question-form" @submit.prevent="updateQuestion">
                        <input type="hidden" name="_method" value="PUT">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="question" class="control-label">Question Heading:</label>
                                        <input type="text" class="form-control" id="question"
                                               name="question"
                                               placeholder="Question Title">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label for="question" class="control-label">Question Image:</label>
                                        <input class="form-control" type="file" name="image">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description" class="control-label">Question Description:</label>
                                <textarea class="form-control summernote" id="description"
                                          name="description"
                                          placeholder="Question Description"></textarea>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info waves-effect">Update</button>
                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close
                            </button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

        <!-- Delete Question modal content -->
        <div id="delete-question-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="vcenter">Delete Question</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <form id="delete-question-form" method="post">
                        <div class="modal-body">
                            Are you sure you want to delete?
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger waves-effect" @click="deleteQuestion">Delete
                            </button>
                            <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal"
                                    @click="removeDeleteId">Cancel
                            </button>
                        </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    </div>
@endsection

@section('page_js')
    @include('admin.exam.js-cdn')
    <script type="text/javascript">
        axios.defaults.headers.common = {
            'X-Requested-With': 'XMLHttpRequest',
            'X-CSRF-TOKEN': '{{ csrf_token() }}'
        };

        new Vue({
            el: "#question_vue",
            data: {
                questions: [],
                question: {
                    exam_id: '{{ $exam->id }}',
                    question: '',
                    image: '',
                    description: ''
                },
                updateQuestionId: null,
                deleteQuestionId: null
            },
            beforeCreate: function () {

            },
            created: function () {
            },
            mounted() {
                this.getQuestions();
            },
            methods: {
                makeImageUrl(image) {
                    let asset = '{{ asset('exam') }}';
                    return `<a href="` + asset + `/` + image + `" target="_blank">Image</a>`;
                },
                getQuestions() {
                    let _this = this;
                    axios.get('{{ route('exam.questions', $exam->id) }}')
                        .then(function (response) {
                            _this.questions = response.data.data.questions;
                        })
                        .catch(function (error) {
                            console.log(error);
                        })
                        .then(function () {
                        });
                },
                createQuestions() {
                    let _this = this;
                    let form = document.getElementById('create-question-form');
                    let formData = new FormData(form);
                    axios.post('{{ route('question.store') }}', formData)
                        .then(function (response) {
                            console.log(response);
                            if (response.data.success) {
                                _this.getQuestions();
                                _this.question.question = '';
                                _this.question.description = '';
                                _this.question.image = '';
                                $('#create-question-form input[name=question]').val('');
                                $('#create-question-form input[name=image]').val(null);
                                $('#create-question-form textarea[name=description]').summernote('code', _this.question.description);
                                window.location.reload();
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
                deleteQuestionModal(id) {
                    this.deleteQuestionId = id;
                    $('#delete-question-modal').modal("show");
                },
                deleteQuestion() {
                    let _this = this;
                    axios.delete('{{ route('question.index') }}/' + _this.deleteQuestionId)
                        .then(function (response) {
                            if (response.data.success) {
                                _this.getQuestions();
                                _this.deleteQuestionId = null;
                                $('#delete-question-modal').modal("hide");
                                window.location.reload();

                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
                removeDeleteId() {
                    this.deleteQuestionId = null;
                },
                deleteImage(id) {
                    let _this = this;
                    axios.post('{{ url("/taskman/question/delete-image") }}/' + id)
                        .then(function (response) {
                            console.log(response);
                            if (response.data.success) {
                                _this.getQuestions();
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
                editQuestion(id) {
                    let _this = this;
                    axios.get('{{ route('question.index') }}/' + id)
                        .then(function (response) {
                            _this.updateQuestionId = id;
                            $('#update-question-modal input[name=question]').val(response.data.data.question);
                            $('#update-question-modal textarea[name=description]').summernote('code', response.data.data.description);
                            window.location.reload();
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
                updateQuestion() {
                    let _this = this;
                    let form = document.getElementById('update-question-form');
                    let formData = new FormData(form);
                    axios.post('{{ route('question.index') }}/' + _this.updateQuestionId, formData)
                        .then(function (response) {
                            if (response.data.success) {
                                _this.getQuestions();
                                _this.updateQuestionId = null;
                                $('#update-question-form input[name=question]').val('');
                                $('#update-question-form input[name=image]').val(null);
                                $('#update-question-form textarea[name=description]').val('');
                                $('#update-question-modal').modal("hide");
                            }
                        })
                        .catch(function (error) {
                            console.log(error);
                        });
                },
            }
        });
    </script>
@endsection
