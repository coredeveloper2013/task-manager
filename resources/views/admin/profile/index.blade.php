@extends('admin.master')

@section('title')
    Mediusware | Profile | {!! $user->name !!}
@endsection

@section('content')
    <style>
        .note-editable.card-block {
            height: 200px !important;
        }
    </style>
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Profile Edit | {!! $user->name !!}</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Profile</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- End Bread crumb and right sidebar toggle -->

    <!-- Container fluid  -->

    <div class="page-content container-fluid">

        @include('admin.include.alert')

        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12">
                                <h4 class="card-title">Profile</h4>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-7 col-md-7 col-xs-12">
                                <form class="" method="post" action="{!! route('profile-update') !!}" novalidate
                                      enctype="multipart/form-data">
                                    @method('PATCH')
                                    @csrf
                                    <div class="row">
                                        <table class="table">
                                        <tr>
                                            <td style="width:200px;">Select Role: </td>
                                            <td>{{ $user->role }}</td>
                                        </tr>
                                        <tr>
                                            <td>Name: </td>
                                            <td>{{$user->name}}</td>
                                        </tr>
                                        <tr>
                                            <td>Emergency Contact Name: </td>
                                            <td>{{ $user->emergency_contact_name }}</td>
                                        </tr>
                                        <tr>
                                            <td>Emergency Contact Number: </td>
                                            <td>{{$user->emergency_contact_number}}</td>
                                        </tr>
                                        <tr>
                                            <td>Email: </td>
                                            <td>{{$user->email}}</td>
                                        </tr>
                                        <tr>
                                            <td>Salary: </td>
                                            <td>{{$user->salary}}</td>
                                        </tr>
                                        <tr>
                                            <td>Bank Details: </td>
                                            <td>{!! $user->bank_details !!}</td>
                                        </tr>
                                        <tr>
                                            <td>Joining Date: </td>
                                            <td>{{$user->joining_date}}</td>
                                        </tr>
                                        <tr>
                                            <td>Permanent Date: </td>
                                            <td>{{$user->permanent_date}}</td>
                                        </tr>
                                        <tr>
                                            <td>Image: </td>
                                            <td><img src="{!! asset('media/user/'. $user->image) !!}" width="100"></td>
                                        </tr>
                                        <tr>
                                            <td>Status: </td>
                                            <td>{{$user->status}}</td>
                                        </tr>
                                        </table>
                                    </div>
                                </form>
                            </div>

                            <div class="col-lg-5 col-md-5 col-xs-12">
                                <form class="" method="post" action="{!! route('update-password') !!}"
                                      novalidate>
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12 form-group">
                                            <h5>Password<span class="text-danger">*</span></h5>
                                            <div class="controls">
                                                <input type="password" name="password" value="{!! old('password') !!}"
                                                       class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                       required
                                                       data-validation-required-message="This field is required">
                                                @if ($errors->has('password'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="col-md-12 text-xs-right">
                                            <button type="submit" class="btn btn-info">Update</button>
                                            <button type="reset" class="btn btn-inverse">Reset</button>
                                        </div>
                                        <hr>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <!-- First Cards Row  -->
    </div>


@endsection
@section('page_js')

@endsection
