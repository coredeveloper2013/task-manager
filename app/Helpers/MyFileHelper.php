<?php

use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Upload File Universal Function
 *
 * @param UploadedFile $file
 * @param string $UploadPath
 * @return \Symfony\Component\HttpFoundation\File\File|bool
 */
function uploadFile(UploadedFile $file, string $UploadPath = 'uploads')
{
    $fileName = time() . ' - ' . $file->getClientOriginalName();
    $uploaded = $file->move(public_path($UploadPath), $fileName);
    if ($uploaded) {
        return $fileName;
    } else {
        return false;
    }
}
