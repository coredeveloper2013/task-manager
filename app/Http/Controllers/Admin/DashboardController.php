<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\User;
use Validator;
use App\Models\Task;
use App\Models\Work;
use App\Models\Leave;
use App\Models\Project;
use App\Models\FcmToken;
use App\Models\MediaImage;
use Illuminate\Http\Request;
use App\Models\LeaveApplication;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class DashboardController extends Controller
{
    public function index()
    {
        $date = date('Y-m-d');

        $tasks = Task::get();
        if (Auth::user()->role == 'employee'){

            $employeeId = Auth::user()->id;
            $taskQueries = Task::where('tasks.status', 'active')->where('tasks.start_date', '<=', $date)->where('tasks.end_date', '>=', $date);
            if ($employeeId){
                $taskQueries->join('task_employee_relations as re_task', 'tasks.id', '=', 're_task.task_id');
                $taskQueries->join('users', 'users.id', '=', 're_task.employee_id');
                $taskQueries->select('tasks.*');
                $taskQueries->where('re_task.employee_id', '=', $employeeId);
            }
            $todayTasks = $taskQueries->get();
            //$todayTasks = Task::where('user_id', Auth::user()->id)->where('start_date', '>=', $date)->get();
        }else{
            $todayTasks = Task::where('start_date', '>=', $date)->get();
        }

        $activeTasks = Task::where('status', 'active')->get();
        $completeTasks = Task::where('status', 'complete')->get();
        $deactivateTasks = Task::where('status', 'deactivate')->get();
        $works = Work::get();
        $todayWorks = Work::where('created_at', 'LIKE', date('Y-m-d').'%')->orderBy('id', 'DESC')->get();
        $todayCompleteWorks = Work::where('status', 'complete')->where('updated_at', 'LIKE', date('Y-m-d').'%')->get();
        $completeWorks = Work::where('status', 'complete')->get();
        $projects = Project::get();

        // User Late report
        $year = date('Y');
        $month = date('m');
        $sql = '';
        if ($year) {
            $sql .= ' AND YEAR(date)='.$year;
        }
        if ($month) {
            $sql .= ' AND MONTH(date)='.$month;
        }
        // Check employee late report
        if (Auth::user()->role == 'employee'){
            $users = User::where('users.id', Auth::user()->id);
        }else{
            $users = User::where('users.role', 'employee');
        }

        $users->select('users.id','users.name','users.image', 'B.in_time_12', 'C.in_time_11', 'D.in_time_10')->where('users.status', 'active');
        $users->leftJoin(\DB::raw("(SELECT COUNT(id) as in_time_12, user_id FROM late_reports WHERE in_time='12' $sql GROUP BY user_id) AS B"), 'users.id', '=', 'B.user_id');
        $users->leftJoin(\DB::raw("(SELECT COUNT(id) as in_time_11, user_id FROM late_reports WHERE in_time='11_12' $sql GROUP BY user_id) AS C"), 'users.id', '=', 'C.user_id');
        $users->leftJoin(\DB::raw("(SELECT COUNT(id) as in_time_10, user_id FROM late_reports WHERE in_time='10_11' $sql GROUP BY user_id) AS D"), 'users.id', '=', 'D.user_id');
        $data = $users->get();

        $leave = Leave::select('leaves.casual_leave', 'leaves.sick_leave', \DB::raw('IFNULL(B.t_casual, 0) AS t_casual'), \DB::raw('IFNULL(C.t_sick, 0) AS t_sick'))
            ->leftJoin(\DB::raw("(SELECT SUM(date_count) as t_casual, user_id FROM leave_applications WHERE YEAR(date_from) >= $year AND YEAR(date_from) <= $year AND leave_type='casual_leave' AND status='approved' GROUP BY user_id) AS B"), 'leaves.user_id', '=', 'B.user_id')
            ->leftJoin(\DB::raw("(SELECT SUM(date_count) as t_sick, user_id FROM leave_applications WHERE YEAR(date_from) >= $year AND YEAR(date_from) <= $year AND leave_type='sick_leave' AND status='approved' GROUP BY user_id) AS C"), 'leaves.user_id', '=', 'C.user_id')
            ->where('leaves.user_id', Auth::user()->id)
            ->where('leaves.year', date('Y'))
            ->first();




        return view('admin.index', compact(
            'tasks', 'works', 'projects', 'activeTasks', 'completeTasks', 'deactivateTasks', 'completeWorks', 'todayTasks',
            'todayWorks', 'todayCompleteWorks', 'year', 'month', 'data', 'leave'
        ));
    }

    public function setting(){
        $setting = Setting::first();
        return view('admin.setting', compact('setting'));
    }


    public function taskImageDelete($id){
        $image = MediaImage::find($id);
        if ($image->image){
            unlink('media/task/'.$image->image);
        }
        $image->delete();
        return redirect()->back();
    }


    public function registerFcmToken(Request $request){

        $fcmToken = FcmToken::updateOrCreate(
            [
                'user_id' => Auth::user()->id,
                'fcm_token' => $request->token
            ]);

        return response()->json([
            'message' => 'success',
            'obj' => $fcmToken,
        ]);
    }

    public function profile()
    {
        $user = User::find(Auth::user()->id);
        return view('admin.profile.index', compact('user'));
    }

    public function updateProfile(Request $request)
    {
        // $data = $request->all();
        // $validator = Validator::make($data, [
        //     'role' => Rule::in(['management', 'employee']),
        //     'name' => 'required|max:191',
        //     'status' => Rule::in(['active', 'deactivate']),
        // ]);
        // if ($validator->fails()) {
        //     return redirect()->back()
        //         ->withErrors($validator)
        //         ->withInput();
        // }
        // $user = User::find(Auth::user()->id);

        // if($request->hasFile('image')) {
        //     if ($user->image){
        //         unlink('media/user/'. $user->image);
        //     }
        //     $image = $request->file('image');
        //     $imageName = $image->getClientOriginalExtension();
        //     $fileName = time() . "." . $imageName;
        //     $directory = 'media/user/';
        //     $imageUrl = $directory.$fileName;
        //     Image::make($image)->resize(300, 300)->save($imageUrl);
        //     $user->image = $fileName;
        // }

        // $user->role    = isset($request->role) ? $request->role:$user->role;
        // $user->name    = $request->name;
        // $user->emergency_contact_name  = isset($request->emergency_contact_name) ? $request->emergency_contact_name:$user->emergency_contact_name;
        // $user->emergency_contact_number  = isset($request->emergency_contact_number) ? $request->emergency_contact_number:$user->emergency_contact_number;
        // $user->about  = isset($request->about) ? $request->about:$user->about;
        // $user->salary  = isset($request->salary) ? $request->salary:$user->salary;
        // $user->bank_details  = isset($request->bank_details) ? $request->bank_details:$user->bank_details;
        // $user->phone  = isset($request->phone) ? $request->phone:$user->phone;
        // $user->joining_date  = isset($request->joining_date) ? $request->joining_date:$user->joining_date;
        // $user->permanent_date  = isset($request->permanent_date) ? $request->permanent_date:$user->permanent_date;
        // $user->status  = isset($request->status) ? $request->status:$user->status;
        // $user->save();
        // return redirect()->back()->with('success', 'Profile update successfully');
    }

    public function updatePassword(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'password' => 'required|max:191|min:6',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $user = User::find(Auth::user()->id);

        $user->password    = Hash::make($request->password);
        $user->save();
        return redirect()->back()->with('success', 'Password update successfully');
    }

}
