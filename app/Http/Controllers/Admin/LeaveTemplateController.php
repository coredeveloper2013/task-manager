<?php

namespace App\Http\Controllers\Admin;

use App\Models\LeaveTemplate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;

class LeaveTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = LeaveTemplate::orderBy('id', 'DESC')->get();
        return view('admin.leave-template.index', compact('records'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.leave-template.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|max:191',
            'template' => 'required',
            'status' => Rule::in(['active', 'deactivate']),
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = new LeaveTemplate();
        $data->name = $request->name;
        $data->template  = $request->template;
        $data->status       = $request->status;
        $data->save();
        return redirect(route('leave-template.index'))->with('success', 'Template save successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = LeaveTemplate::find($id);
        return response()->json([
            'message' => 'success',
            'obj' => $data,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = LeaveTemplate::find($id);
        return view('admin.leave-template.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required|max:191',
            'template' => 'required',
            'status' => Rule::in(['active', 'deactivate']),
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $data = LeaveTemplate::find($id);
        $data->name = $request->name;
        $data->template  = $request->template;
        $data->status       = $request->status;
        $data->save();
        return redirect(route('leave-template.index'))->with('success', 'Template update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        LeaveTemplate::find($id)->delete();
        return redirect(route('leave-template.index'))->with('error', 'Template delete successfully');
    }
}
