<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EmployeeNote extends Model
{
    protected $fillable = [
      'user_id', 'comment', 'date'
    ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
}
