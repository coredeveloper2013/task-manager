<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TaskEmployeeRelation extends Model
{
    protected $table = 'task_employee_relations';

    public function user(){
        return $this->belongsTo('App\User', 'employee_id', 'id');
    }

    public function task(){
        return $this->belongsTo('App\Models', 'task_id', 'id');
    }
}
