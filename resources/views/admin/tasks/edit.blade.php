@extends('admin.master')

@section('title')
    Mediusware | Task | {!! $task->name !!} | Edit
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Task Create</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('task.index') !!}">Tasks</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

        @include('admin.include.alert')

        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10 col-sm-8">
                                <h4 class="card-title">Task Edit</h4>
                            </div>
                            <div class="col-md-2 col-sm-4 text-right">
                                <a href="{!! route('task.index') !!}" class="btn btn-success"><i class="fa fa-arrow-left"></i> Back</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <form class="" method="post" action="{!! route('task.update', $task->id)!!}" novalidate enctype="multipart/form-data">
                            @method('PATCH')
                            @csrf
                            <div class="row">

                                <div class="col-lg-7 col-md-7 col-xs-12">
                                    <div class="form-group">
                                        <h5>Select Employee<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <select class="select2 form-control{{ $errors->has('employee') ? ' is-invalid' : '' }}" name="employee[]" multiple="multiple" style="height: 36px;width: 100%;" required data-validation-required-message="This field is required">
                                                @if(!empty($employees))
                                                    @foreach($employees as $employee)
                                                        <option value="{!! $employee->id !!}" {{ (in_array($employee->id, $task->employees))?'selected':'' }}>
                                                            {!! $employee->name !!}
                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>
                                            @if ($errors->has('employee'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('employee') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Select Project<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <select name="project" class="form-control{{ $errors->has('project') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                                @foreach($projects as $project)
                                                    <option value="{!! $project->id !!}" {!! $project->id == $task->project_id ? 'selected':'' !!}>{!! $project->project_name !!}</option>
                                                @endforeach
                                            </select>
                                            @if ($errors->has('project'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('project') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Task Name<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="text" name="name" value="{!! old('name', $task->name) !!}" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                            @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Task Details<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <textarea  name="task_details"  class="form-control{{ $errors->has('task_details') ? ' is-invalid' : '' }} summernote" required data-validation-required-message="This field is required">{!! old('task_details', $task->task_details) !!}</textarea>
                                            @if ($errors->has('task_details'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('task_details') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <h5>Start Date<span class="text-danger">*</span></h5>
                                                <div class="input-group">
                                                    <input type="text" class="form-control{{ $errors->has('start_date') ? ' is-invalid' : '' }} datepicker-autoclose" value="{!! old('start_date',  $task->start_date) !!}" required data-validation-required-message="This field is required" name="start_date"  placeholder="Start Date">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="icon-calender"></i></span>
                                                    </div>
                                                    @if ($errors->has('start_date'))
                                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('start_date') }}</strong>
                                                </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <h5>End Date<span class="text-danger">*</span></h5>
                                                <div class="input-group">
                                                    <input type="text" class="form-control{{ $errors->has('end_date') ? ' is-invalid' : '' }} datepicker-autoclose" value="{!! old('end_date',  $task->end_date) !!}" required data-validation-required-message="This field is required" name="end_date"  placeholder="End Date">
                                                    <div class="input-group-append">
                                                        <span class="input-group-text"><i class="icon-calender"></i></span>
                                                    </div>
                                                    @if ($errors->has('end_date'))
                                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('end_date') }}</strong>
                                                </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Task Note</h5>
                                        <div class="controls">
                                            <textarea  name="note"  class="form-control{{ $errors->has('note') ? ' is-invalid' : '' }}" >{!! old('note',  $task->note) !!}</textarea>
                                            @if ($errors->has('note'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('note') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>


                                    <div class="form-group validate">
                                        <h5>Status <span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" {!! $task->status == 'active' ? 'checked':'' !!} name="status" value="active" required="" id="status1" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="status1">Active</label>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" {!! $task->status == 'deactivate' ? 'checked':'' !!}  name="status" value="deactivate" id="status" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="status">Deactivate</label>
                                                </div>
                                            </fieldset>
                                            <fieldset>
                                                <div class="custom-control custom-radio">
                                                    <input type="radio" {!! $task->status == 'complete' ? 'checked':'' !!}  name="status" value="complete" id="status2" class="custom-control-input" aria-invalid="false">
                                                    <label class="custom-control-label" for="status2">Complete</label>
                                                </div>
                                            </fieldset>
                                            @if ($errors->has('status'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('status') }}</strong>
                                                </span>
                                            @endif
                                            <div class="help-block"></div>
                                        </div>
                                    </div>
                                    <hr>

                                </div>
                                <div class="col-lg-5 col-md-5 col-xs-12">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="form-group">
                                                <h5>Task List<span class="text-danger">*</span></h5>
                                                <div class="controls">
                                                    <input type="text" name="task_list[]" value="{!! old('task_list') !!}" placeholder="" class="form-control{{ $errors->has('task_list') ? ' is-invalid' : '' }}">
                                                    @if ($errors->has('task_list'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('task_list') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-2">
                                            <br>
                                            <div class="form-group">
                                                <button class="btn btn-success" type="button" onclick="feature_added();"><i class="fa fa-plus"></i></button>
                                            </div>
                                        </div>
                                    </div>

                                    @if(!empty($task->taskLists))
                                        @php $i = 1 @endphp
                                        @foreach($task->taskLists as $key => $list)
                                            <div class="form-group removeclass{{$i}}">
                                                <div class="row">
                                                    <div class="col-md-10">
                                                        <div class="form-group">
                                                            <div class="controls">
                                                                <input type="text" name="task_list[]" value="{!! old('task_list', $list->list_name) !!}" placeholder="" class="form-control{{ $errors->has('task_list') ? ' is-invalid' : '' }}" required data-validation-required-message="This field is required">
                                                                @if ($errors->has('task_list'))
                                                                    <span class="invalid-feedback" role="alert">
                                                                        <strong>{{ $errors->first('task_list') }}</strong>
                                                                    </span>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2">
                                                        <div class="form-group">
                                                            <button class="btn btn-danger" type="button" onclick="remove_feature({!! $i !!});"><i class="fa fa-minus"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @php $i++ @endphp
                                        @endforeach
                                    @endif

                                    <div id="education_fields"></div>

                                    <hr>

                                    <div class="form-group">
                                        <h5>Task Image<span class="text-danger">*</span></h5>
                                        <div class="controls">
                                            <input type="file" name="images[]" multiple>
                                            @if ($errors->has('images'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('images') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <hr><br>
                                    <div class="row">
                                        @if(!empty($task->images))
                                            @foreach($task->images as $image)
                                                <div class="col-md-6 text-center mb-5">
                                                    <img src="{!! asset('media/task/'. $image->image) !!}"class="img-fluid shadow-lg">
                                                    <a href="{!! url('taskman/media/image/'.$image->id) !!}/delete" style="margin-top: 10px!important;" class="btn btn-danger"> <i class="fa fa-trash"></i></a>
                                                </div>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="text-xs-right">
                                <button type="submit" class="btn btn-info">Update</button>
                                <button type="reset" class="btn btn-inverse">Reset</button>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>

        <!-- First Cards Row  -->
    </div>


@endsection

@section('page_js')

    <script>
        var inc = 1;

        function feature_added() {
            inc++;
            var objTo = document.getElementById('education_fields')
            var divtest = document.createElement("div");
            divtest.setAttribute("class", "form-group removeclass" + inc);
            var rdiv = 'removeclass' + inc;
            divtest.innerHTML =
                '<div class="row">'+
                '<div class="col-md-10">'+
                '<div class="form-group">'+
                '<div class="controls">'+
                '<input type="text" name="task_list[]" value="" placeholder="" class="form-control" required data-validation-required-message="This field is required">'+

                '</div>'+
                '</div>'+
                '</div>'+

                '<div class="col-sm-2"> ' +
                '<div class="form-group"> ' +
                '<button class="btn btn-danger" type="button" onclick="remove_feature(' + inc + ');"> <i class="fa fa-minus"></i> </button> ' +
                '</div>' +
                '</div>'+
                '</div>';

            objTo.appendChild(divtest)
        }

        function remove_feature(rid) {
            $('.removeclass' + rid).remove();
        }
    </script>

    <script>
        $(document).ready(function() {
            $('input[name="images[]"]').fileuploader({
                addMore: true,
                files: null,
            });
        });

    </script>
@endsection
