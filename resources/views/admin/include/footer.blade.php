<footer class="footer text-center">
    Copyright {!! date('Y') !!} © All Rights Reserved
    <a href="https://mediusware.com">Mediusware</a>
</footer>