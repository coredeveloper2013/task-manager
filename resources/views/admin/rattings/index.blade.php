@extends('admin.master')

@section('title')
    Mediusware | Rattings
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Rattings</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Rattings</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>

    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

    @include('admin.include.alert')
    <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">All Rattings</h4>
                            </div>
                            <div class="col-md-2 text-right">
                                <a href="{!! route('rattings.create') !!}" class="btn btn-success"><i
                                        class="fa fa-plus-circle"></i> Add New</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>
                        <form action="{!! url('taskman/rattings') !!}" method="get">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>Date</h5>
                                        <div class="input-group">
                                            <input type="text"
                                                   class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }} datepicker-autoclose"
                                                   value="{!! !empty($data) ? $data['date']:"" !!}"
                                                   name="date" placeholder="Date">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>Select Employee</h5>
                                        <div class="controls">
                                            <select class="form-control" name="user_id">
                                                <option value="">All Employees</option>
                                                @foreach($employees as $user)
                                                    <option
                                                        value="{!! $user->id !!}" {!! !empty($data) && $data['user_id'] == $user->id ? 'selected':''  !!}>{!! $user->name !!}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>Select Ratting</h5>
                                        <div class="controls">
                                            <select
                                                class="form-control{{ $errors->has('ratting') ? ' is-invalid' : '' }}"
                                                name="ratting">
                                                <option value=""> Select Ratting</option>
                                                @for ($star = 1; $star <= 10; $star++)
                                                    <option
                                                        {!! !empty($data) && $data['ratting'] == $star ? 'selected':''  !!} value="{{ $star }}"> {{ $star }}
                                                        *
                                                    </option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <br>
                                        <div class="controls">
                                            <button type="submit" class="btn btn-info">Go</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <div class="table-responsive">
                            <table id="row_create_call" class="table table-striped table-hover table-bordered display"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th>Date</th>
                                    <th>Manager Name</th>
                                    <th>Employee | Ratting | Commnets</th>
                                    <th width="0%">Action</th>
                                </tr>
                                </thead>
                                <tbody id="tablecontents">
                                @foreach($rattings as $key => $ratting)
                                    <tr class="row1" data-id="{{ $ratting->id }}">
                                        <td>{!! ++$key !!}</td>
                                        <td>{!! date('F d, Y', strtotime($ratting->date)) !!}</td>
                                        <td>{!! $ratting->user->name !!}</td>
                                        <td>
                                            @if(!empty($ratting->details))
                                                @foreach($ratting->details as $key=> $emp)
                                                    <span>{{ $key+1 }}. {!! $emp->user->name !!} | Ratting: {{ $emp->ratting }}  {{ $emp->comment != "" ? "| comments: ". $emp->comment : ""  }}  </span>
                                                    <br>
                                                @endforeach
                                            @endif
                                        </td>
                                        <td>
                                            <form action="{{ route('rattings.destroy', $ratting->id)}}" method="post">
                                                @csrf
                                                @method('DELETE')
                                                <a href="{{ route('rattings.edit', $ratting->id)}}"
                                                   class="btn btn-info btn-circle"><i class="fa fa-edit"></i> </a>
                                                <button type="submit" onclick="return confirm('Are you sure...?')"
                                                        class="btn btn-danger btn-circle"><i class="fa fa-trash"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


@endsection

@section('page_js')
    <script type="text/javascript">
        $(function () {

        });
    </script>
@endsection
