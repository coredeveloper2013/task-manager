<?php

namespace App\Http\Controllers\Admin;

use App\Models\Ratting;
use App\Models\RattingDetail;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rule;
use Validator;
use Auth;

class RattingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = $request->all();

        $rattings = Ratting::with(['details']);
        if ($request->input('user_id')) {
            $rattings = $rattings->whereHas('details', function ($query) use ($request) {
                $query->where('user_id', $request->user_id);
            });
        }
        if ($request->input('ratting')) {
            $rattings = $rattings->whereHas('details', function ($query) use ($request) {
                $query->where('ratting', $request->ratting);
            });
        }
        if ($request->input('date')) {
            $rattings->where('date', $request->date);
        }

        $rattings = $rattings->get();


//        $query = Ratting::with(['details']);
//        if (!empty($data['date'])) {
//            $query->where('date', $data['date']);
//        }
//        if (!empty($data['user_id'])) {
//            $query->whereHas('details', function ($q) use ($data) {
//                $q->where('user_id', $data['user_id']);
//            });
//        }
//
//        $rattings = $query->get();
//        dd($rattings);
        $employees = User::where('role', 'employee')
            ->where('status', 'active')
            ->orderBy('sort', 'ASC')
            ->get();
        return view('admin.rattings.index', compact('data', 'rattings', 'employees'));
    }

    public function report(Request $request)
    {
        $data = $request->all();

        $rattings = RattingDetail::with(['rattingMaster']);
        if ($request->input('manager_id')) {
            $rattings = $rattings->whereHas('rattingMaster', function ($query) use ($request) {
                $query->where('user_id', $request->manager_id);
            });
        }
        if (!empty($request->input('start_date')) && !empty($request->input('end_date'))) {
            $rattings = $rattings->whereHas('rattingMaster', function ($query) use ($request) {
                $query->whereBetween('date', [$request->start_date, $request->end_date]);
//                $query->where('date', $request->date);
            });
        }
        if ($request->input('user_id')) {
            $rattings->where('user_id', $request->user_id);
        }
        if ($request->input('ratting')) {
            $rattings->where('ratting', $request->ratting);
        }

        $rattings = $rattings->get();
//dd($rattings);
        $employees = User::where('role', 'employee')
            ->where('status', 'active')
            ->orderBy('sort', 'ASC')
            ->get();
        $managers = User::where('role', 'management')
            ->where('status', 'active')
            ->orderBy('sort', 'ASC')
            ->get();
        return view('admin.rattings.report', compact('data', 'rattings', 'employees', 'managers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $employees = User::where('role', 'employee')
            ->where('status', 'active')
            ->orderBy('sort', 'ASC')
            ->get();
        return view('admin.rattings.create', compact('employees'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//        $validator = Validator::make($request, [
//            'date' => 'required|date',
//        ]);
//        if ($validator->fails()) {
//            return redirect()->back()
//                ->withErrors($validator)
//                ->withInput();
//        }

        $input = $request->except('_token');

        $obj = new Ratting();
        $obj->user_id = Auth::user()->id;
        $obj->date = $request->date;
        if ($obj->save()) {
            if (!empty($input['rattings'])) {
                foreach ($input['rattings'] as $ratting) {
                    if (isset($ratting["employee_id"])) {
                        $obj->details()->create([
                            'user_id' => $ratting["employee_id"],
                            'ratting' => $ratting["ratting"],
                            'comment' => $ratting["comment"],
                        ]);
                    }
                }
            }
        }

        return redirect(route('rattings.index'))->with('success', 'Rattings Save Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = Task::find($id);
        $reviews = Review::where('task_id', $id)->orderBy('date', 'DESC')->get();
        $upReview = Review::where('task_id', $id)->where('date', date('Y-m-d'))->first();
        return view('admin.rattings.view', compact('task', 'reviews', 'upReview'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ratting = Ratting::findOrFail($id);
        $employees = User::where('role', 'employee')
            ->where('status', 'active')
            ->orderBy('sort', 'ASC')
            ->get();
        return view('admin.rattings.edit', compact('ratting', 'employees'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

//        $validator = Validator::make($request, [
//            'date' => 'required|date',
//        ]);
//        if ($validator->fails()) {
//            return redirect()->back()
//                ->withErrors($validator)
//                ->withInput();
//        }

        $input = $request->except('_token');

        $obj = Ratting::findOrFail($id);
        $obj->date = $request->date;
        if ($obj->update()) {
            if (!empty($input['rattings'])) {
                $data = [];
                $row11 = RattingDetail::where('ratting_id', $obj->id)->get();
                foreach ($row11 as $data12) {
                    $array_id[] = $data12->id;
                }
                foreach ($input['rattings'] as $ratting) {
                    if (!empty($ratting["employee_id"])) {
                        $detail_id[] = isset($ratting["detail_id"]) && !empty($ratting["detail_id"] ? $ratting["detail_id"] : '');
                    }
                }
                if (!empty($array_id) && $detail_id) {
                    $remove_data = array_diff($array_id, $detail_id);
                    RattingDetail::whereIn('id', $remove_data)->delete();
                }
                foreach ($input['rattings'] as $ratting) {
                    if (!empty($ratting["employee_id"])) {
                        $data = array(
                            'ratting_id' => $obj->id,
                            'user_id' => $ratting["employee_id"],
                            'ratting' => $ratting["ratting"],
                            'comment' => $ratting["comment"],
                        );
                        $row = RattingDetail::find($ratting["detail_id"]);
                        if (!empty($row)) {
                            RattingDetail::where('id', $ratting["detail_id"])->update($data);
                        } else {
                            RattingDetail::insert($data);
                        }
                    }

                }

//                    if (isset($ratting["employee_id"])) {
//                        $obj->details()->create(
////                            ['id' => $ratting["detail_id"],],
//                            [
//                            'user_id' => $ratting["employee_id"],
//                            'ratting' => $ratting["ratting"],
//                            'comment' => $ratting["comment"],
//                        ]);
//                    }
            }
        }

        return redirect(route('rattings.index'))->with('success', 'Ratting Update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public
    function destroy($id)
    {
        $obj = Ratting::findOrFail($id);
        $obj->details()->delete();
        $obj->delete();
        return redirect()->back()->with('error', 'Ratting Delete Successfully');
    }

}
