<?php

namespace App\Http\Controllers\Admin;

use App\Models\Applicant;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ApplicantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $applicant = Applicant::findOrFail($id);
        if ($request->approval == 1) {
            Mail::send('admin.exam.email', compact('applicant'), function ($msg) use ($request) {
                $msg->from('info@mediusware.com', 'MediusWare.Com');
                $msg->subject("Interview Applicant");
                $msg->to(env('INTERVIEW_RECEIVER'));
            });
        }
        $update = $applicant->update([
            'approval' => $request->approval,
            'remark' => $request->remark
        ]);
        if ($update) {
            session()->flash('success', 'Remarking successfully');
        } else {
            session()->flash('error', 'Remarking Failed');
        }
        return redirect()->route('exam.show-examine', $applicant->exam_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function remark($id)
    {

        $data = Applicant::find($id);
        if ($data) {
            return response()->json(['status' => 'success', 'data' => $data]);
        }
        return response()->json(['status' => 'fail']);
    }
}
