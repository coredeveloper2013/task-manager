<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SalaryController extends Controller
{
    public function index(Request $request){

        if ($request->year>0) {
            $year = $request->year;
        } else {
            $year = date('Y');
        }

        if ($request->month>0) {
            $month = $request->month;
        } else {
            $month = date('m');
        }

        $sql = '';
        if ($year) {
            $sql .= ' AND YEAR(date)='.$year;
        }
        if ($month) {
            $sql .= ' AND MONTH(date)='.$month;
        }

        $users = User::where('users.role','!=', 'admin')->where('users.status', 'active');
        $users->select('users.id','users.name','users.image','users.salary', \DB::raw('IFNULL(B.in_time_12, 0) AS in_time_12'), \DB::raw('IFNULL(C.in_time_11, 0) AS in_time_11'), \DB::raw('IFNULL(D.in_time_10, 0) AS in_time_10'));
        $users->leftJoin(\DB::raw("(SELECT COUNT(id) as in_time_12, user_id FROM late_reports WHERE in_time='12' $sql GROUP BY user_id) AS B"), 'users.id', '=', 'B.user_id');
        $users->leftJoin(\DB::raw("(SELECT COUNT(id) as in_time_11, user_id FROM late_reports WHERE in_time='11_12' $sql GROUP BY user_id) AS C"), 'users.id', '=', 'C.user_id');
        $users->leftJoin(\DB::raw("(SELECT COUNT(id) as in_time_10, user_id FROM late_reports WHERE in_time='10_11' $sql GROUP BY user_id) AS D"), 'users.id', '=', 'D.user_id');
        $salaries = $users->get();

        return view('admin.salary.index', compact('salaries', 'month', 'year'));
    }

    public function yearEnd(Request $request){

        if ($request->year>0) {
            $year = $request->year;
        } else {
            $year = date('Y');
        }

        $sql = '';
        if ($year) {
            $sql .= ' AND YEAR(date)='.$year;
        }
        $sql .= ' AND MONTH(date)=12';

        $users = User::where('users.role','!=', 'admin')->where('users.status', 'active');
        $users->select('users.id','users.name','users.image','users.salary', \DB::raw('IFNULL(B.in_time_12, 0) AS in_time_12'), \DB::raw('IFNULL(C.in_time_11, 0) AS in_time_11'), \DB::raw('IFNULL(D.in_time_10, 0) AS in_time_10'), \DB::raw('IFNULL(E.t_casual, 0) AS t_casual'), 'F.casual_leave');
        $users->leftJoin(\DB::raw("(SELECT COUNT(id) as in_time_12, user_id FROM late_reports WHERE in_time='12' $sql GROUP BY user_id) AS B"), 'users.id', '=', 'B.user_id');
        $users->leftJoin(\DB::raw("(SELECT COUNT(id) as in_time_11, user_id FROM late_reports WHERE in_time='11_12' $sql GROUP BY user_id) AS C"), 'users.id', '=', 'C.user_id');
        $users->leftJoin(\DB::raw("(SELECT COUNT(id) as in_time_10, user_id FROM late_reports WHERE in_time='10_11' $sql GROUP BY user_id) AS D"), 'users.id', '=', 'D.user_id');
        $users->leftJoin(\DB::raw("(SELECT SUM(date_count) as t_casual, user_id FROM leave_applications WHERE YEAR(date_from) >= $year AND YEAR(date_from) <= $year AND leave_type='casual_leave' AND status='approved' GROUP BY user_id) AS E"), 'users.id', '=', 'E.user_id');
        $users->leftJoin(\DB::raw("(SELECT casual_leave, user_id FROM leaves WHERE year=$year) AS F"), 'users.id', '=', 'F.user_id');
        $salaries = $users->get();

        return view('admin.salary.year-end', compact('salaries', 'month', 'year'));
    }
}
