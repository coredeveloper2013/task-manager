<?php

if (!function_exists('fcmFire')) {
    function fcmFire($post){
        $post['notification']['tag'] = [];
        $post['notification']['tag'] = uniqid();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://fcm.googleapis.com/fcm/send",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($post, true),
            CURLOPT_HTTPHEADER => array(
                "authorization: key=AAAAvjCsM4Y:APA91bFcZxiYmKJwofgygqm7LbWXyuuglXFIXAkc_EafOseqilEMLkbc1b2vEe7jXtBAzwzjGsvq5a-NOCyOSUByKlDvGWWSYSE3O3D8CVePpBfiypsfo4bQ56mKbhcrEbTkdR4uOme-",
                "content-type: application/json",
            ),
        ));
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        if ($err) {
            return $err;
        } else {
            return $response;
        }
    }
}

if (!function_exists('getUserFcmToken')) {
    function getUserFcmToken($userId){
        $userToken = \App\Models\FcmToken::where('user_id', $userId)->get();
        return $userToken;
    }
}
