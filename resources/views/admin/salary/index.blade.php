@extends('admin.master')

@section('title')
    Mediusware | Salary
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Salary</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Salary</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    
    
    <!-- Container fluid  -->

    <div class="page-content container-fluid">

    @include('admin.include.alert')
        <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">All Employee Salary</h4>
                            </div>
                        </div>
                        <form action="{!! url('taskman/salary') !!}" method="get">
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>Select Month</h5>
                                        <div class="controls">
                                            <select class="form-control" name="month" required data-validation-required-message="This field is required">
                                                @for($i = 1 ; $i <= 12; $i++)
                                                    <option value="{!! date("m",strtotime(date("Y")."-".$i."-01")) !!}" {!! $month == date("m",strtotime(date("Y")."-".$i."-01")) ? 'selected':'' !!}>
                                                        {!! date("F",strtotime(date("Y")."-".$i."-01")) !!}
                                                    </option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>Select Year</h5>
                                        <div class="controls">
                                            <select class="form-control" name="year" required data-validation-required-message="This field is required">
                                                @for($i = date('Y') ; $i >= 2019; $i--)
                                                    <option value="{!! $i !!}" {!! $year == $i ? 'selected':'' !!}>
                                                        {!! $i !!}
                                                    </option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <br>
                                        <div class="controls">
                                            <button type="submit" class="btn btn-info">Go</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered display" style="width:100%;">
                                <thead>
                                <tr>
                                    <th width="8%">Avatar</th>
                                    <th>Name</th>
                                    <th>Salary</th>
                                    <th>10 to 11</th>
                                    <th>11 to 12</th>
                                    <th>After 12</th>
                                    <th>Total Salary</th>
                                </tr>
                                </thead>
                                <tbody id="tablecontents">
                                @if(count($salaries)>0)
                                    @foreach($salaries as $user)
                                        @php
                                            $salaryPerDay   = $user->salary / 30;
                                            $late_10_to_11  = floor($user->in_time_10 / 3) * $salaryPerDay;
                                            $late_11_to_12  = floor($user->in_time_11  / 2) * $salaryPerDay;;
                                            $late_after_12  = $user->in_time_12  / 2 * $salaryPerDay;;
                                            $totalLate      = $late_10_to_11 + $late_11_to_12 + $late_after_12;
                                            $totalSalary    = $user->salary - $totalLate
                                        @endphp

                                        <tr>
                                            <td class="text-center">
                                                @if($user->image)
                                                    <img src="{!! asset('media/user/'.$user->image) !!}" width="50">
                                                @else
                                                    <img src="{!! asset('avatar.png') !!}" width="50">
                                                @endif
                                            </td>
                                            <td>{!! $user->name !!}</td>
                                            <td>Tk. {!! $user->salary !!}</td>
                                            <td>
                                                @if($late_10_to_11 > 0)
                                                    {!! "-".floor($late_10_to_11). " ($user->in_time_10) " !!}
                                                @else
                                                   {!! floor($late_10_to_11). " ($user->in_time_10) " !!}
                                                @endif
                                            </td>
                                            <td>
                                                @if($late_11_to_12 > 0)
                                                    {!! "-".floor($late_11_to_12). " ($user->in_time_11) " !!}
                                                @else
                                                    {!! floor($late_11_to_12). " ($user->in_time_11) " !!}
                                                @endif
                                            </td>
                                            <td>
                                                @if($late_after_12 > 0)
                                                    {!! "-".floor($late_after_12). " ($user->in_time_12) " !!}
                                                @else
                                                    {!! floor($late_after_12). " ($user->in_time_12) " !!}
                                                @endif
                                            </td>

                                            <td>
                                                {!! 'Tk.'.floor($totalSalary) !!}
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8" align="center">Data not found!</td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('page_js')

@endsection
