<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRattingDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ratting_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ratting_id');
            $table->integer('user_id');
            $table->integer('project_id')->nullable();
            $table->integer('ratting');
            $table->longText('comment')->nullable();
            $table->foreign('ratting_id')
                ->references('id')->on('rattings')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ratting_details');
    }
}
