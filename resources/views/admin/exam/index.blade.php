@extends('admin.master')

@section('title')
    Mediusware | Interview Exams
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Exams</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Exams</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->
    <!-- Container fluid  -->
    <div class="page-content container-fluid">

    @include('admin.include.alert')
    <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">All Exams</h4>
                            </div>
                            <div class="col-md-2 text-right">
                                <button class="btn btn-success" data-toggle="modal" data-target="#create-exam-modal"><i
                                        class="fa fa-plus-circle"></i> Add New
                                </button>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-bordered display our-dataTable"
                                   style="width:100%">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Exam Title</th>
                                    <th>Created At</th>
                                    <th class="text-center">Status</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($exams as $key => $exam)
                                    <tr>
                                        <td>{!! ++$key !!}</td>
                                        <td>{!! $exam->title !!}</td>
                                        <td>{!! $exam->created_at->format('d-m-Y h:m A') !!}</td>
                                        <td class="text-center">
                                            @if($exam->status == 1)
                                                <span style="font-size: 16px;" class="badge badge-pill badge-success">Active</span>
                                            @else($exam->status == 0)
                                                <span style="font-size: 16px;" class="badge badge-pill badge-danger">Inactive</span>
                                            @endif
                                        </td>
                                        <td class="float-right">
                                            <a href="{{ route('exam.show-examine', $exam->id) }}"
                                               class="btn btn-info btn-circle"
                                               data-toggle="tooltip" data-placement="top" title="Show Examine List">
                                                <i class="fas fa-user"></i>
                                            </a>
                                            @if($exam->questions->count() > 0)
                                                <a href="{{ route('interview-exam.show', $exam->slug) }}"
                                                   class="btn btn-circle btn-danger" target="_blank"
                                                   data-toggle="tooltip" data-placement="top" title="Share Examine Link">
                                                    <i class="fas fa-share-alt-square"></i>
                                                </a>
                                            @else
                                                <button class="btn btn-circle btn-danger" disabled="disabled"
                                                   data-toggle="tooltip" data-placement="top" title="Share Examine Link">
                                                    <i class="fas fa-share-alt-square"></i>
                                                </button>
                                            @endif

                                            <a href="{{ route('exam.show', $exam->id) }}"
                                               class="btn btn-info btn-circle"
                                               data-toggle="tooltip" data-placement="top" title="View Questions">
                                                <i class="fas fa-question"></i>
                                            </a>
                                            <button type="button" class="btn btn-info btn-circle update-button"
                                                    data-toggle="modal" data-placement="top" title="Update Questions"
                                                    data-target="#update-exam-modal" data-id="{{ $exam->id }}">
                                                <i class="fa fa-edit"></i>
                                            </button>
                                            <button type="button" class="btn btn-info btn-circle"
                                                    onclick="updateStatus({{ $exam->id }})"
                                                    data-toggle="tooltip" data-placement="top" title="Change Status">
                                                <i class="fas fa-eye"></i>
                                            </button>
                                            <button type="button" class="btn btn-circle btn-danger"
                                                    onclick="deleteExamModal({{ $exam->id }})"
                                                    data-toggle="tooltip" data-placement="top" title="Delete">
                                                <i class="fas fa-trash-alt"></i>
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Create Exam modal content -->
    <div id="create-exam-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="vcenter">Create Exam</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form action="{{ route('exam.store') }}" method="post">
                    <div class="modal-body">
                        @csrf
                        <div class="form-group">
                            <label for="exam-title" class="control-label">Exam Title:</label>
                            <input type="text" class="form-control" id="exam-title" name="title"
                                   placeholder="Exam Title">
                        </div>
                        <div class="form-group">
                            <label for="exam-comprehensive" class="control-label">Exam Comprehensive:</label>
                            <textarea class="form-control summernote" id="comprehensive"
                                      name="comprehensive"
                                      placeholder="Exam Comprehensive"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info waves-effect">Create</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Update Exam modal content -->
    <div id="update-exam-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="vcenter">Update Exam</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form id="update-exam-form" method="post">
                    <div class="modal-body">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="exam-title" class="control-label">Exam Title:</label>
                            <input type="text" class="form-control" id="exam-title" name="title"
                                   placeholder="Exam Title">
                        </div>
                        <div class="form-group">
                            <label for="exam-comprehensive" class="control-label">Exam Comprehensive:</label>
                            <textarea class="form-control summernote" id="comprehensive"
                                      name="comprehensive"
                                      placeholder="Exam Comprehensive"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info waves-effect">Update</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <!-- Delete Exam modal content -->
    <div id="delete-exam-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="vcenter">Delete Exam</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <form id="delete-exam-form" method="post">
                    <div class="modal-body">
                        Are you sure you want to delete?
                        @csrf
                        @method('DELETE')
                        <input type="hidden" class="form-control" name="id">
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-danger waves-effect">Delete</button>
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Cancel
                        </button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    @if(Session::has('hasApplicant'))
        <!-- Delete Exam Error modal content -->
        <div id="delete-exam-error-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="vcenter"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="vcenter">Info</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                    </div>
                    <div class="modal-body">
                        <p class="text-danger">{{ Session::get('hasApplicant') }}</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary waves-effect" data-dismiss="modal">Ok
                        </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
    @endif

@endsection

@section('page_js')
    <script type="text/javascript">
        function updateStatus(id) {
            let data = {
                _token: '{{ csrf_token() }}'
            };
            $.ajax({
                type: 'post',
                url: '{{ url("taskman/exam/change-activity") }}/' + id,
                cache: false,
                data: data,
                success: function (response) {
                    if (response.success) {
                        window.location.href = "{{ route('exam.index') }}";
                    }
                }
            });
        }

        function deleteExamModal(id) {
            $('#delete-exam-form').attr('action', '{{ url("taskman/exam") }}/' + id);
            $('#delete-exam-form input[name="id"]').val(id);
            $('#delete-exam-modal').modal("show");
        }

        $('.update-button').click(function () {
            let id = $(this).attr('data-id');
            $('#update-exam-form').attr('action', '{{ url("taskman/exam") }}/' + id);
            $.ajax({
                url: '{{ url("taskman/exam") }}/' + id + '/edit',
                cache: false,
                success: function (response) {
                    if (response.success) {
                        $('#update-exam-form input[name="title"]').val(response.data.title);
                        $('#update-exam-form textarea[name="comprehensive"]').summernote('code', response.data.comprehensive);
                    }
                }
            });
        });

        $('.our-dataTable').DataTable();

        @if(Session::has('hasApplicant'))
            $('#delete-exam-error-modal').modal("show");
        @endif
    </script>
@endsection
