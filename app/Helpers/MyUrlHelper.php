<?php

function makeUrl($text)
{
    $text = str_replace('http://www.', 'http://', $text);
    $text = str_replace('https://www.', 'https://', $text);
    $text = str_replace('www.', 'http://', $text);
//    return preg_replace('@(https?://([-\w\.]+)+(:\d+)?(/([-\w/_\.]*(\?\S+)?)?)?)@', '<a href="$1">$1</a>', $text);
    $pattern = '/(?:(?:https?|ftp|file):\/\/|www\.|ftp\.)(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[-A-Z0-9+&@#\/%=~_|$?!:,.])*(?:\([-A-Z0-9+&@#\/%=~_|$?!:,.]*\)|[A-Z0-9+&@#\/%=~_|$])/i';
    return preg_replace($pattern, '<a target="_blank" style="color: #05a0ff; font-weight: 500;" href="$0">$0</a>', $text);
}
