@extends('admin.master')

@section('title')
    Mediusware | Interview Exams
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Answer Paper</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">
                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Answer Paper</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->

    <!-- Container fluid  -->
    <div class="page-content container-fluid">

    @include('admin.include.alert')
    <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">{!! $exam->title !!}</h4>
                            </div>
                            <div class="col-md-2 text-right">

                            </div>
                            <div class="col-12">
                                <div class="question-body">
                                    <div class="container">
                                        <div class="question-paper">
                                            <div class="c-logo">
                                                <img src="{{asset('logo-small.png')}}" alt="">
                                            </div>
                                            <div class="exam-name"></div>

                                            <div class="card c-card">
                                                <div class="card-header c-header">
                                                    <div>Examinee Information</div>
                                                </div>
                                                <div class="card-body c-body">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <div class="form-group row">
                                                                <label
                                                                    class="col-3 col-form-label c-label">Name: </label>
                                                                <div class="col-9">
                                                                    <P>{{ $applicant->name }}</P>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label
                                                                    class="col-3 col-form-label c-label">Email: </label>
                                                                <div class="col-9">
                                                                    <p>{{ $applicant->email }}</p>
                                                                </div>
                                                            </div>
                                                            <div class="form-group row">
                                                                <label
                                                                    class="col-3 col-form-label c-label">Phone: </label>
                                                                <div class="col-9">
                                                                    <p>{{ $applicant->phone }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-6">
                                                            <div class="form-group row">
                                                                <div class="col-12">
                                                                    <label
                                                                        class="col-3 col-form-label c-label">Address: </label>
                                                                    <div class="col-9">
                                                                        <p>{{ $applicant->address }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @foreach($exam->questions as $key => $question)

                                                @if($question->applicantAnswer !== null)
                                                    <div class="card c-card no-margin bottom-shadow">
                                                        <div class="card-header c-header">
                                                            <div>Q. {{ $question->question }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group cmb-0" style="margin-bottom: -1rem;">
                                                            <p class="form-control q-control">
                                                                {!! ($question->applicantAnswer) ? makeUrl($question->applicantAnswer->answer) : '' !!}
                                                            </p>
                                                    </div>
                                                @elseif($question->applicantAnswer == null)
                                                    <div class="card c-card no-margin bottom-shadow">
                                                        <div class="card-header c-header" style="background: red">
                                                            <div>Q. {{ $question->question }}</div>
                                                        </div>
                                                    </div>
                                                    <div class="form-group cmb-0" style="margin-bottom: -1rem;display: none;">

                                                    </div>
                                                @endif

                                                {{--Null check--}}
                                                @if($question->description !== null)
                                                    <div class="ques-detail">
                                                        <span class="text-danger" style="font-weight: 700">N.B.</span>
                                                        <span class="detail-nb">
                                                            {!! makeUrl($question->description) !!}
                                                        </span>
                                                    </div>
                                                @endif
                                                <div class="margin-10"></div>
                                                <div class="margin-10"></div>
                                                <div class="margin-10"></div>
                                            @endforeach
                                            {{-- Comnent box --}}
                                            <form method="post" class="form-horizontal"
                                                  action="{{ route('applicant.update',$applicant->id) }}"
                                                  enctype="multipart/form-data">
                                                @csrf
                                                @method('PUT')
                                                <div class="card c-card no-margin bottom-shadow">
                                                    <div class="card-header c-header">
                                                        <div for="remark">Remark</div>
                                                    </div>
                                                    <div class="form-group cmb-0">
                                                     <textarea class="form-control q-control" name="remark"
                                                               id="remark" rows="3"
                                                               placeholder="Write your remark">{{ $applicant->remark }}</textarea>
                                                    </div>
                                                </div>
                                                {{--Approval--}}
                                                <div class="card c-card">
                                                    <div class="form-group cmb-0">
                                                        <label class="col-3 col-form-label c-label">Approval: </label>
                                                        <div class="col-9">

                                                            <input type="radio" value="1" id="approval"
                                                                   name="approval" {{$applicant->approval == 1 ? 'checked' : ''}}>
                                                            <label class="checkbox-inline" for="approval">Approve</label>

                                                            <input type="radio" value="2" id="approval1"
                                                                   name="approval" {{($applicant->approval == 2 || $applicant->approval == 0)? 'checked' : ''}} style="margin-left: 10px;">
                                                            <label class="checkbox-inline" for="approval1">Decline</label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="margin-10"></div>
                                                <div class="margin-10"></div>
                                                <div class="margin-10"></div>
                                                <button type="submit" class="btn btn-primary btn-theme float-right"
                                                        style="width: 20%;">Submit
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
@section('page_js')
    <script>
        $('.our-dataTable').DataTable();
    </script>

@endsection
