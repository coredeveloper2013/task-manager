<?php

namespace App\Http\Controllers\Admin;

use App\Models\Applicant;
use App\Models\ApplicantAnswer;
use App\Models\Exam;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ApplicantAnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param string $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|void
     */
    public function show($slug)
    {
        $exam = Exam::with('questions')->where('slug', $slug)->first();
        if ($exam) {
            $examOpen = Exam::with('questions')->where('slug', $slug)->where('status', 1)->first();
            if ($examOpen) {
                $examOpen = true;
                return view('exam.interview.questionPaper', compact('exam', 'examOpen'));
            } else {
                $examOpen = false;
                return view('exam.interview.questionPaper', compact('exam', 'examOpen'));
            }
        } else {
            return abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Submit Answer Script
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function submitAnswer(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);

        $checkIfExit = Applicant::where('exam_id', $id)->where('email', $request->email)->first();
        if ($checkIfExit) {
            session()->flash('error', 'You have already participated on this exam!');
            return redirect()->back()->withInput($request->input());
        }

        $applicant = Applicant::create([
            'exam_id' => $id,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'address' => $request->address,
        ]);

        if ($applicant) {
            foreach ($request->answer as $key => $answer) {
                if (isset($answer)) {
                    ApplicantAnswer::create([
                        'exam_id' => $id,
                        'applicant_id' => $applicant->id,
                        'question_id' => $key,
                        'answer' => $answer,
                    ]);
                }
            }
        }
        session()->flash('success', 'Answer submitted successfully');

        return redirect()->back();
    }
}
