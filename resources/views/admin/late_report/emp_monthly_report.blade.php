@extends('admin.master')

@section('title')
    Mediusware | Late Report
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Late Report</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Late Report</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    
    <!-- End Bread crumb and right sidebar toggle -->
    
    <!-- Container fluid  -->

    <div class="page-content container-fluid">

    @include('admin.include.alert')
        <!-- First Cards Row  -->
        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10">
                                <h4 class="card-title">All employees late report</h4>
                            </div>
                            <div class="col-md-2 text-right">
                                <a href="{!! url('taskman/emp-late-report/list') !!}" class="btn btn-success"><i class="fa fa-backward"></i> Back</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>
                        <form action="{!! route('emp-late-report.monthly') !!}" method="get">
                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>Select Month</h5>
                                        <div class="controls">
                                            <select class="form-control" name="month" required data-validation-required-message="This field is required">
                                                @for($i = 1 ; $i <= 12; $i++)
                                                    <option value="{!! date("m",strtotime(date("Y")."-".$i."-01")) !!}" {!! $month == date("m",strtotime(date("Y")."-".$i."-01")) ? 'selected':'' !!}>
                                                        {!! date("F",strtotime(date("Y")."-".$i."-01")) !!}
                                                    </option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <h5>Select Year</h5>
                                        <div class="controls">
                                            <select class="form-control" name="year" required data-validation-required-message="This field is required">
                                                @for($i = date('Y') ; $i >= 2019; $i--)
                                                    <option value="{!! $i !!}" {!! $year == $i ? 'selected':'' !!}>
                                                        {!! $i !!}
                                                    </option>
                                                @endfor
                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <br>
                                        <div class="controls">
                                            <button type="submit" class="btn btn-info">Go</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>

                        <div class="table-responsive">
                            <table id="row_create_call" class="table table-striped table-hover table-bordered display" style="width:100%">
                                <thead>
                                <tr>
                                    <th width="8%">Avatar</th>
                                    <th>Name</th>
                                    <th>10 to 11</th>
                                    <th>11 to 12</th>
                                    <th>After 12</th>
                                </tr>
                                </thead>
                                <tbody id="tablecontents">

                                @foreach($data as $user)
                                    <tr>
                                        <td class="text-center">
                                            @if($user->image)
                                                <img src="{!! asset('media/user/'.$user->image) !!}" width="50">
                                            @else
                                                <img src="{!! asset('avatar.png') !!}" width="50">
                                            @endif
                                        </td>
                                        <td>{!! $user->name !!}</td>
                                        <td>
                                            @if($user->in_time_10 == 1)
                                                {!! $user->in_time_10. ' Day'  !!}
                                            @else
                                                {!! isset($user->in_time_10) ? $user->in_time_10.' Days':'0 Day'  !!}
                                            @endif
                                        </td>
                                        <td>
                                            @if($user->in_time_11 == 1)
                                                {!! $user->in_time_11.' Day'  !!}
                                            @else
                                                {!! isset($user->in_time_11) ? $user->in_time_11.' Days':'0 Day'  !!}
                                            @endif
                                        </td>
                                        <td>
                                            @if($user->in_time_12 == 1)
                                                {!! $user->in_time_12.' Day'  !!}
                                            @else
                                                {!! isset($user->in_time_12) ? $user->in_time_12.' Days':'0 Day'  !!}
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




@endsection

@section('page_js')

@endsection
