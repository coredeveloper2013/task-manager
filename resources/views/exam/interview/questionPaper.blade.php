<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" type="image" sizes="32x32" href="{{ asset('admin/images/favicon.png') }}">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <!-- Toaster -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">

    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.1.4/toastr.min.css">

    <link rel="stylesheet" href="{{asset('examCSS/exam.css')}}">

    <title>Mediusware | Exam Paper</title>
</head>
<body>
<div class="question-body">
    <div class="container">
        <div class="question-paper">
            <div class="c-logo">
                <img src="{{asset('logo-small.png')}}" alt="">
            </div>
            <div class="exam-name">{{$exam->title}}</div>
            @if (!$examOpen)
                <div class="card c-card">
                    <div class="card-header c-header">
                        <div>Exam is closed!</div>
                    </div>
                    <div class="card-body c-body">
                    </div>
                </div>
            @else
                @if ($exam->comprehensive)
                    <div class="card c-card">
                        <div class="card-header c-header">
                            <div>Exam Comprehensive</div>
                        </div>
                        <div class="card-body c-body">
                            {!! $exam->comprehensive !!}
                        </div>
                    </div>
                @endif
                <form method="post" class="form-horizontal" action="{{ route('interview-exam.submit',$exam->id) }}"
                      enctype="multipart/form-data">
                    @csrf
                    @include('exam.interview._messages')
                    <div class="card c-card">
                        <div class="card-header c-header">
                            <div>Examinee Information</div>
                        </div>
                        <div class="card-body c-body">
                            <div class="row">
                                <div class="col-12 col-md-6 ">
                                    <div class="form-group row">
                                        <label for="name" class="col-12 col-md-3 col-form-label c-label">Name <span
                                                style="color: red">*</span></label>
                                        <div class="col-12 col-md-9">
                                            <input type="text" name="name"
                                                   class="form-control c-control {{ $errors->has('name') ? 'is-invalid' : ''}}"
                                                   id="name" placeholder="Name" value="{{ old('name') }}" required>
                                            @if($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="email" class="col-12 col-md-3 col-form-label c-label">Email <span
                                                style="color: red">*</span></label>
                                        <div class="col-12 col-md-9">
                                            <input type="email" name="email"
                                                   class="form-control c-control {{ $errors->has('email') ? 'is-invalid' : ''}}"
                                                   id="email" placeholder="Email" value="{{ old('email') }}" required>
                                            @if($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="phone" class="col-12 col-md-3 col-form-label c-label">Phone <span
                                                style="color: red">*</span></label>
                                        <div class="col-12 col-md-9">
                                            <input type="text" name="phone"
                                                   class="form-control c-control {{ $errors->has('phone') ? 'is-invalid' : ''}}"
                                                   id="phone" placeholder="Phone" value="{{ old('phone') }}" required>
                                            @if($errors->has('phone'))
                                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6">
                                    <div class="form-group row">
                                        <div class="col-12">
                                <textarea class="form-control c-control i-height" name="address" id="address"
                                          placeholder="Address"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @foreach($exam->questions as $key => $question)
                        <div class="card c-card no-margin bottom-shadow">
                            <div class="card-header c-header">
                                <div class="row">
                                    @if ($question->image)
                                        <div class="col-10">
                                            <div class="text-justify">Q. {{$question->question}}</div>
                                        </div>
                                        <div class="col-2 text-right">
                                            <a href="{{ asset('exam').'/'.$question->image }}" target="_blank"
                                               class="btn btn-secondary">Attachment</a>
                                        </div>
                                    @else
                                        <div class="col-12">
                                            <div class="text-justify">Q. {{$question->question}}</div>
                                        </div>
                                    @endif

                                </div>
                            </div>
                        </div>
                        <div class="form-group cmb-0">
                        <textarea class="form-control q-control" name="answer[{{ $question->id }}]"
                                  id="answer[{{ $question->id }}]" rows="3"
                                  placeholder="Write your answer">{{ old('answer') ? old('answer')[$question->id] : '' }}</textarea>
                        </div>
                        {{--Null check--}}
                        @if($question->description !== null)
                            <div class="ques-detail">
                                <span class="text-danger" style="font-weight: 700">N.B.</span> <br>
                                <span class="detail-nb">
                                    {!! makeUrl($question->description) !!}
                                </span>
                            </div>
                        @endif

                        <div class="margin-10"></div>
                        <div class="margin-10"></div>
                        <div class="margin-10"></div>
                    @endforeach
                    <button type="submit" class="btn btn-primary btn-theme float-right" style="width: 20%;">Submit
                    </button>
                </form>
            @endif
        </div>
    </div>
</div>
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
        integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
        integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
        integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
        crossorigin="anonymous"></script>
</body>
</html>
