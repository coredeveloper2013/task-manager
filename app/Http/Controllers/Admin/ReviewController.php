<?php

namespace App\Http\Controllers\Admin;

use App\Models\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;

class ReviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $todayReview =  Review::where('work_id', $request->work_id)->where('date', date('Y-m-d'))->first();
        if (!empty($todayReview)){
            return redirect()->back()->with('error', 'Today you already reviewed. You can try update!');
        }
        $data = $request->all();
        $validator = Validator::make($data, [
            'rating' => Rule::in(['1', '2', '3', '4', '5']),
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $review = new Review();
        $review->work_id     = $request->work_id;
        $review->task_id     = $request->task_id;
        $review->employee_id = $request->employee_id;
        $review->rating      = $request->rating;
        $review->comments    = $request->comments;
        $review->date        = date('Y-m-d');
        $review->save();
        return redirect()->back()->with('success', 'Your review save successfully!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $review = Review::find($id);
        return response()->json([
            'message' => 'success',
            'obj' => $review,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $validator = Validator::make($data, [
            'rating' => Rule::in(['1', '2', '3', '4', '5']),
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $review = Review::find($id);
        $review->rating     = $request->rating;
        $review->comments   = $request->comments;
        $review->save();
        return redirect()->back()->with('success', 'Your review update successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Review::find($id)->delete();
        return redirect()->back()->with('error', 'Your review delete successfully!');
    }
}
