@extends('admin.master')

@section('title')
    Mediusware | Ratting / Review Create
@endsection

@section('content')
    <!-- Bread crumb and right sidebar toggle -->
    <div class="page-breadcrumb border-bottom">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-xs-12 align-self-center">
                <h5 class="font-medium text-uppercase mb-0">Ratting / Review Create</h5>
            </div>
            <div class="col-lg-9 col-md-8 col-xs-12 align-self-center">

                <nav aria-label="breadcrumb" class="mt-2 float-md-right float-left">
                    <ol class="breadcrumb mb-0 justify-content-end p-0">
                        <li class="breadcrumb-item"><a href="{!! url('/home') !!}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('home') !!}">Dashboard</a></li>
                        <li class="breadcrumb-item"><a href="{!! route('rattings.index') !!}">Rattings</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Create</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
    <!-- End Bread crumb and right sidebar toggle -->


    <!-- Container fluid  -->

    <div class="page-content container-fluid">

        @include('admin.include.alert')

        <div class="row">
            <div class="col-12">
                <div class="material-card card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-10 col-sm-8">
                                <h4 class="card-title">Ratting / Review Create</h4>
                            </div>
                            <div class="col-md-2 col-sm-4 text-right">
                                <a href="{!! route('rattings.index') !!}" class="btn btn-success"><i
                                        class="fa fa-arrow-left"></i> Back</a>
                            </div>
                            <div class="col-12">
                                <hr>
                            </div>
                        </div>

                        <form class="" method="post" action="{!! route('rattings.store') !!}" novalidate
                              enctype="multipart/form-data">
                            @csrf
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <h5>Date<span class="text-danger">*</span></h5>
                                        <div class="input-group">
                                            <input type="text"
                                                   class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }} datepicker-autoclose"
                                                   value="{!! old('date', date('Y-m-d')) !!}" required
                                                   data-validation-required-message="This field is required"
                                                   name="date" placeholder="Date">
                                            <div class="input-group-append">
                                                <span class="input-group-text"><i class="icon-calender"></i></span>
                                            </div>
                                            @if ($errors->has('date'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('date') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-lg-12 col-md-12 col-xs-12">
                                    <div class="table-responsive">
                                        <table id="row_create_call"
                                               class="table table-striped table-hover table-bordered display"
                                               style="width:100%">
                                            <thead>
                                            <tr>
                                                <th>Employee</th>
                                                <th>Ratting</th>
                                                <th>Commnets</th>
                                            </tr>
                                            </thead>
                                            <tbody id="tablecontents">
                                            @foreach($employees as $key=> $employee)
                                                <tr class="row1" data-id="{{ $employee->id }}">
                                                    <td>
                                                        <label for="employee_id_{{$key}}">
                                                            <input value="{{ $employee->id }}" id="employee_id_{{$key}}"
                                                                   type="checkbox"
                                                                   name="rattings[{{$key}}][employee_id]"> {{ $employee->name }}

                                                        </label>
                                                    </td>
                                                    <td>
                                                        <div class="controls">
                                                            <select
                                                                class="form-control{{ $errors->has('ratting') ? ' is-invalid' : '' }}"
                                                                name="rattings[{{$key}}][ratting]"
                                                                data-validation-required-message="This field is required">
                                                                @for ($star = 1; $star <= 10; $star++)
                                                                    <option value="{{ $star }}"> {{ $star }}*</option>
                                                                @endfor
                                                            </select>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="controls">
                                                        <textarea name="rattings[{{$key}}][comment]"
                                                                  class="form-control{{ $errors->has('comment') ? ' is-invalid' : '' }}">{!! old('note') !!}</textarea>
                                                            @if ($errors->has('note'))
                                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('comment') }}</strong>
                                                </span>
                                                            @endif
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <div class="text-xs-right">
                                    <button type="submit" class="btn btn-info">Submit</button>
                                    <button type="reset" class="btn btn-inverse">Reset</button>
                                </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>

        <!-- First Cards Row  -->
    </div>


@endsection

@section('page_js')
    <script type="text/javascript">
    </script>
@endsection
