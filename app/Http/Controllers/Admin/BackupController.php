<?php

namespace App\Http\Controllers\Admin;

use App\Models\DbBackup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Validator;

class BackupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $records = DbBackup::orderBy('id', 'DESC')->get();
        return view('admin.db-backup.index', compact('records'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = DbBackup::find($id);
        $data->delete();
        unlink(public_path().'/backupdb/'.$data->export_file);
        return redirect(route('db-backup.index'))->with('error', 'Backup DB delete successfully');
    }
}
