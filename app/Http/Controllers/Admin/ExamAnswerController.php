<?php

namespace App\Http\Controllers\Admin;

use App\Models\Applicant;
use App\Models\Exam;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ExamAnswerController extends Controller
{
    /**
     * Show the answers which belongs to the applicant ID
     *
     * @param int $applicant_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAnswers($applicant_id)
    {
        $applicant = Applicant::findOrFail($applicant_id);
        $exam = Exam::with(['questions' => function ($q) use ($applicant_id) {
            $q->with(['applicantAnswer' => function ($q) use ($applicant_id) {
                $q->where('applicant_id', $applicant_id);
            }]);
        }])->find($applicant->exam_id);

        return view('admin.exam.examinee.ansShow', compact('applicant', 'exam'));
    }
}
