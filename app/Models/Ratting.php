<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Ratting extends Model
{
//    protected $table = 'rattings';
    protected $guarded = ['id'];


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function details()
    {
        return $this->hasMany(RattingDetail::class);
    }
}
