<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Applicant extends Model
{
    protected $guarded = ['id'];

    public function exam()
    {
        return $this->belongsTo(Exam::class);
    }
    public function applicantAnswer()
    {
        return $this->hasMany(ApplicantAnswer::class);
    }
}
